

const Task = require('../module/task');




module.exports.addTask = (req, res) => {              
            Task.add(req.body,(err, response) => {
                if (err) {
                    res.json({ status: 0, msg: "something went wrong", data: err });

                } else {
                    res.json({ status: 1, msg: "task addedd successfully", data: { taskId: response.insertId } });
                }
            });
        
   
}

module.exports.listTask = (req, res) => {
    let data = req.body;
    Task.list((err, response) => {
        if (err) {
            res.json({ status: 0, msg: "something went wrong", data: err });

        } else {
            res.json({ status: 1, msg: "task list", data: response });
        }
    });
}
module.exports.updateTask = (req, res) => {
    Task.update(req.body,(err, response) => {
        if (err) {
            res.json({ status: 0, msg: "something went wrong", data: err });

        } else {
            res.json({ status: 1, msg: "task updated successfully", data: { taskId: response } });
        }
    });
}

module.exports.markCompleted = (req, res) => {
    Task.updateComplete(req.body,(err, response) => {
        if (err) {
            res.json({ status: 0, msg: "something went wrong", data: err });

        } else {
            res.json({ status: 1, msg: "task updated successfully", data: { taskId: response } });
        }
    });
}

module.exports.deleteTask = (req, res) => {
    let data = req.body;
    Task.delete(data,(err, response) => {
        if (err) {
            res.json({ status: 0, msg: "something went wrong", data: err });

        } else {
            res.json({ status: 1, msg: "task deleted successfully", data: { taskId: response } });
        }
    });
}