const express = require('express');
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(express.json());
const Task = require('./controller/task');
  app.post('/add-task',Task.addTask);
  app.get('/get-task',Task.listTask);
  app.post('/update-task',Task.updateTask);
  app.post('/delete-task',Task.deleteTask);
  app.post('/mark-complete',Task.markCompleted);
  app.listen(8000, function () {
    console.log("Server running at Port 8000");
  });
