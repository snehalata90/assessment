const conn = require('../module/dbConnection');
var moment = require('moment');

module.exports.add = (data,callback) => {

    let taskName = (data.taskName) ? data.taskName : '';
    let taskDate = (data.taskDate) ? moment(data.taskDate,'DD/MM/YYYY').format("YYYY-MM-DD") : '';
    let taskTime = (data.taskTime) ? moment(data.taskTime,'HH:mm:ss').format("HH:mm:ss") : '';
    if(taskName!=''&& taskDate!='' && taskTime!=''){
        let sql = "INSERT INTO tbl_task(taskName,taskDate,taskTime,createdDateTime) VALUES(?,?,?,now())";
        conn.query(sql,[taskName,taskDate,taskTime], (err, row) => {
            if (err) {
                callback(err, null);
    
            } else {
                callback(null, row);
            }
        });
    }else{
        callback('please provide task name,date and time', null);
    }

}

module.exports.list = (callback) => {
    conn.query(`SELECT taskName,taskDate,taskTime,createdDateTime FROM tbl_task WHERE deleteFlag=0`, (err, row) => {
        if (err) {
            callback(err, null);

        } else {
            if (row.length > 0) {
                Object.keys(row).forEach((key) => {
                    Object.keys(row[key]).forEach((rowDataKey) => {
                        if (row[key][rowDataKey] == null) {
                            row[key][rowDataKey] = '';
                        }

                    });
                });
            }
            callback(null, row);
        }
    });
}
module.exports.delete = (data,callback) => {
    let taskId = (data.taskId) ? data.taskId : '';
    if(taskId!=''){
        const sql = "UPDATE tbl_task SET deleteFlag=0 where taskId=?"
        conn.query(sql,[taskId], (err, row) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, taskId);
            }
        });
    }else{
        callback('please provide task to be deleted', null);
    }
    
}

module.exports.update = (data,callback) => {
    let taskId = (data.taskId) ? data.taskId : '';
    let taskName = (data.taskName) ? data.taskName : '';
    let taskDate = (data.taskDate) ? moment(data.taskDate,'DD/MM/YYYY').format("YYYY-MM-DD") : '';
    let taskTime = (data.taskTime) ? moment(data.taskTime,'HH:mm:ss').format("HH:mm:ss") : null;
    if(taskId!=''){
        const sql = "UPDATE tbl_task SET taskName=?,taskDate=?,taskTime=?,modifiedDateTime=now() WHERE taskId=?";
        conn.query(sql,[taskName,taskDate,taskTime,taskId], (err, row) => {
            if (err) {
                callback(err, null);
    
            } else {
                callback(null, taskId);
            }
        });
    }else{
        callback('please provide task to be updated', null);
    }
   
}

module.exports.updateComplete = (data,callback) => {
    let taskId = (data.taskId) ? data.taskId : '';
   if(taskId!=''){
        const sql = "UPDATE tbl_task SET status='complete' WHERE taskId=?";
        conn.query(sql,[taskId], (err, row) => {
            if (err) {
                callback(err, null);
    
            } else {
                callback(null, taskId);
            }
        });
    }else{
        callback('please provide task to be updated', null);
    }
   
}